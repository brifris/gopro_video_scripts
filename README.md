# gopro_video_scripts

| script | function |
| ------ | ------ |
| pipeline | The main script that calls the others. Start Here to at least see how the others are used.  Unfortunately this won't work as-is because it depends on another python module youtube_selenium_uploader that I modified slightly.  I need to refactor how that is used. |
| boxscore | overlays a boxscore image and then adds teams, scores, outs, innings at the appropriate times based on RTGA (real-time game annotation) output. |
| details | Wrapper around ffprobe to dump the details that are most interesting to me. |
| filter | typically used to reduce frame rate and resolution of a video or all videos in a directory |
| gdrive | get/put videos to Google Drive |
| highlight | extracts video segments from based on a yaml file that defines the segments.  The tag script is what produces/manipulates this yaml file.  It also has the ability to select segments based on criteria (subject, action, quality). |
| merge | Merge the 4GB videos produced by GoPro based on naming convention and sequence numbers. |
| tag | Intended to be called from a VLC plugin that I wrote to tag segments of video (start, stop, etc).  The plugin launches this script from keyboard shortcuts. |
