#!/usr/bin/python3

import argparse, datetime, os, glob, re, subprocess, sys, yaml

#
# manifest format notes:
# overlay: True    # causes 'note' field to overlay as text on the video segment
# convert:
#   scale: 1920x1080    # frame size
#   frame_rate: 30      # frames per second
#   crop: "w:h:x:y"     # crop to width:height starting at x:y (from upper left)
#

def main():
    parser = argparse.ArgumentParser(description='Overlay Box Scores')
    parser.add_argument('--config', type=str, default='boxscore.yaml', help='boxscore configuration yaml')
    parser.add_argument('--skip_existing', action='store_true', help='skip processing files where output already exists')
    parser.add_argument('rtga_yaml', type=str, help='RTGA input events yaml file')
    parser.add_argument('input_video', type=str, help='input video file')
    parser.add_argument('output_video', type=str, help='output video file')
    args = parser.parse_args()

    if os.path.exists(args.output_video):
        if not args.skip_existing:
            raise Exception("Output video already exists!  {}".format(args.output_video))
        print("skipping because output video already exists: {}".format(args.output_video))
    else:
        overlay_boxscore(args)


def overlay_boxscore(args):
    """
ffmpeg -i test.mp4 -i 720p-scorebox-template.png -filter_complex "[0:v][1:v]overlay,drawtext=fontfile=/usr/share/fonts/truetype/freefont/FreeSans.ttf:text=5 Star:fontcolor=white:fontsize=18:x=1068:y=644,drawtext=fontfile=/usr/share/fonts/truetype/freefont/FreeSans.ttf:text=Other Guys:fontcolor=white:fontsize=18:x=1068:y=682,drawtext=fontfile=/usr/share/fonts/truetype/freefont/FreeSans.ttf:text=4:fontcolor=white:fontsize=18:x=1180:y=644,drawtext=enable='between(t,20,40)':fontfile=/usr/share/fonts/truetype/freefont/FreeSans.ttf:text=2:fontcolor=white:fontsize=18:x=1180:y=682[v]" -map [v] -map 0:a -vcodec libx264 test2.mp4
    """
    
    with open(args.config, 'r') as fin:
        config = yaml.load(fin, Loader=yaml.Loader)
        
    with open(args.rtga_yaml, 'r') as fin:
        rtga = yaml.load(fin, Loader=yaml.Loader)
        
    cmd = ['ffmpeg', '-i', args.input_video, '-i', config['boxscore']['template']]
    if 'preset' in config['boxscore']:
        cmd.append('-preset')
        cmd.append(config['boxscore']['preset'])
    if 'frame_rate' in config['boxscore']:
        cmd.append('-r')
        cmd.append(str(config['boxscore']['frame_rate']))
    cmd.append('-filter_complex')
    filter = []
    if 'scale' in config['boxscore']:
        filter.append('[0:v]scale={}:flags=bicubic[vs];'.format(config['boxscore']['scale']))
        filter.append('[vs][1:v]overlay')
    else:
        filter.append('[0:v][1:v]overlay')
    
    font = config['boxscore']['font']
    font_file = font['file']
    home_name = config['boxscore']['home']['name']
    home_score = config['boxscore']['home']['score']
    visitor_name = config['boxscore']['visitor']['name']
    visitor_score = config['boxscore']['visitor']['score']
    inning = config['boxscore']['inning']
    outs = config['boxscore']['outs']

    #last_event_start = 0
    events_list = rtga['state']['events'] 
    #for event in events['state']['events']:
    for i in range(len(events_list)):
        event = events_list[i]
        event_start = time_to_tag(event['at'])
        if i < len(events_list) - 1:
            event_end = time_to_tag(events_list[i + 1]['at'])
        else:
            event_end = 999999999
        # Home Name & Score
        filter.append(',drawtext=enable=\'between(t,{},{})\':fontfile={}:fontcolor={}:fontsize={}:text={}:x={}:y={}'.format(event_start, event_end, font['file'], font['color'], font['size'], rtga['home'], home_name['x'], home_name['y']))
        filter.append(',drawtext=enable=\'between(t,{},{})\':fontfile={}:fontcolor={}:fontsize={}:text={}:x={}:y={}'.format(event_start, event_end, font['file'], font['color'], font['size'], event['home'], home_score['x'], home_score['y']))

        # Visitor Name & Score
        filter.append(',drawtext=enable=\'between(t,{},{})\':fontfile={}:fontcolor={}:fontsize={}:text={}:x={}:y={}'.format(event_start, event_end, font['file'], font['color'], font['size'], rtga['visitor'], visitor_name['x'], visitor_name['y']))
        filter.append(',drawtext=enable=\'between(t,{},{})\':fontfile={}:fontcolor={}:fontsize={}:text={}:x={}:y={}'.format(event_start, event_end, font['file'], font['color'], font['size'], event['visitor'], visitor_score['x'], visitor_score['y']))
        
        # Inning & Outs
        filter.append(',drawtext=enable=\'between(t,{},{})\':fontfile={}:fontcolor=black:fontsize={}:text={}:x={}:y={}'.format(event_start, event_end, font['file'], font['size'], event['inning'], inning['x'], inning['y']))
        if 'outs' in event:
            filter.append(',drawtext=enable=\'between(t,{},{})\':fontfile={}:fontcolor=black:fontsize={}:text={}:x={}:y={}'.format(event_start, event_end, font['file'], font['size'], event['outs'] + ' Out', outs['x'], outs['y']))
        
        #last_event_start = event_start
    
    filter.append('[v]')
    
    cmd.append(''.join(filter))
    cmd.append('-map')
    cmd.append('[v]')
    cmd.append('-map')
    cmd.append('0:a')
    cmd.append(args.output_video)
    
    print(cmd)
    subprocess.run(cmd, shell=False, check=True)


def tag_to_time(tag):
    # convert floating point time tag to 00:00:00 time format
    dt = datetime.datetime.fromtimestamp(float(tag), datetime.timezone.utc)
    return dt.strftime("%H:%M:%S") 


def time_to_tag(timestr):
    # convert 00:00:00 time format to floating point time tag
    return (datetime.datetime.strptime(timestr, '%H:%M:%S') - datetime.datetime(1900, 1, 1)).total_seconds()


if __name__ == '__main__':
    main()
    
