#!/usr/bin/python3

import argparse, os, glob, re, subprocess, sys

def main():
    parser = argparse.ArgumentParser(description='Cleanup already merged GoPro videos')
    parser.add_argument('--video_path', type=str, default='.', help='directory path to all of the videos')
    #parser.add_argument('--manifest', type=str, help='manifest text file containing filenames of video parts pre-merge. created by gopro_video_merge.py')
    parser.add_argument('--force', type=bool, default=False, help='delete without prompting for confirmation')
    parser.add_argument('manifest', type=str, help='manifest text file containing filenames of video parts pre-merge. created by gopro_video_merge.py')
    args = parser.parse_args()

    confirm_video_exists(args)
    files = build_files_to_remove(args)
    remove_files(args, files)
    
    
def confirm_video_exists(args):
    manifest = os.path.join(args.video_path, args.manifest)
    video = manifest
    if video.endswith(".txt"):
        video = video.replace(".txt", "")
    video += ".mp4"
    if not os.path.exists(video):
        print("Could not find video file at {}".format(video))
        print("It looks like you are about to delete files that were never merged!!!")
        sys.exit(1)
        
    
def build_files_to_remove(args):
    files = []
    manifest = os.path.join(args.video_path, args.manifest)
    with open(manifest, 'r') as fin:
        for line in fin:
            line = line.strip()
            m = re.match(r"file '([^']+)'", line)
            if m and os.path.exists(m[1]):
                files.append(m[1])
    return files


def confirm():
    yes = {'yes','y', 'ye', ''}
    no = {'no','n'}
    while True:
        choice = input().lower()
        if choice in yes:
           return True
        elif choice in no:
           return False
        else:
           sys.stdout.write("Please respond with 'yes' or 'no'")


def remove_files(args, files):
    print("These files are about to be deleted!!!")
    for file in files:
        print(file)
    print("Are you sure you want to delete them? (Y/N) ")
    if confirm():
        print("Lets do it!")
        for file in files:
            os.remove(file)   
        print("Cleanup Complete")

    
if __name__ == '__main__':
    main()

