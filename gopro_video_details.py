#!/usr/bin/python3

import argparse, os, glob, re, subprocess, sys, yaml

def main():
    parser = argparse.ArgumentParser(description='Show Video Details')
    parser.add_argument('video', type=str, help='video file')
    args = parser.parse_args()

    show_video_details(args)
    

def show_video_details(args):
    cmd_args = ['ffprobe', '-v', 'error', '-show_format', '-show_streams', args.video]
    print(cmd_args)
    subprocess.run(cmd_args, shell=False, check=True)


if __name__ == '__main__':
    main()
    
