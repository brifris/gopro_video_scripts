#!/usr/bin/python3

import argparse, os, glob, re, subprocess, sys

def main():
    parser = argparse.ArgumentParser(description='Filter and Scale GoPro videos for upload to YouTube')
    parser.add_argument('--video_path', type=str, default='.', help='directory path to all of the videos')
    parser.add_argument('--output_path', type=str, default='', help='output directory path, defaults to same as video_path')
    parser.add_argument('--all', action='store_true', help='process all high res videos in directory')
    parser.add_argument('--skip_existing', action='store_true', help='skip processing files where output already exists')
    parser.add_argument('--res', type=str, default='720p', help='output resolution: 480p, 720p, 1080p (default 720p)')
    parser.add_argument('--frame_rate', type=str, default='30', help='output frame rate (default 30)')
    parser.add_argument('--fast', action='store_true', help='uses ffmpeg superfast encoding mode to try and speed up processing, but creates larger files')
    parser.add_argument('video_files', type=str, nargs='*', help='video file to process')
    args = parser.parse_args()

    files = build_files_to_process(args)
    print(files)
    for file in files:
        filter_video(args, file)
    
    
def build_files_to_process(args):
    files = []
    if args.all:
        for file in sorted(glob.glob('*-high.mp4')):
            files.append(file)
    for file in args.video_files:
        files.append(file)
    return files


def filter_video(args, input_file):
    if args.res == '1080p':
        scale = '1920x1080'
    elif args.res == '720p':
        scale = '1280x720'
    elif args.res == '480p':
        scale = '854x480'
    else:
        raise Exception("Unsupported output resolution of " + args.res)
                
    output_file = input_file.replace('-high', '-' + args.res)
    if args.output_path:
        output_file = os.path.join(args.output_path, os.path.basename(output_file))
    if input_file == output_file:
        raise Exception("Input file and Output file cannot have the same name!")

    if os.path.exists(output_file):
        if args.skip_existing:
            print("Skipping {} because it already exists".format(output_file))
        else:
            raise Exception("Output file {} already exists!".format(output_file))
    else:
        preset = 'medium'
        if args.fast:
            preset = 'superfast'
        cmd_args = ['ffmpeg', '-i', input_file, '-preset', preset, '-s', scale, '-r', args.frame_rate, '-c:a', 'copy', output_file]
        print(cmd_args)
        subprocess.run(cmd_args, shell=False, cwd=args.video_path, check=True)



if __name__ == '__main__':
    main()
    
