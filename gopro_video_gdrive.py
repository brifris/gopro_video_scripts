#!/usr/bin/python3

import argparse, os, sys, json, yaml
from urllib.parse import urlparse
import datetime

from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive


def main():
    parser = argparse.ArgumentParser(description='Get/Send videos to google drive')
    parser.add_argument('--folder', type=str, help='folder of video files')
    
    subparsers = parser.add_subparsers(title='subcommands', description='valid subcommands')
    getparser = subparsers.add_parser('get', description='Get Videos')
    putparser = subparsers.add_parser('put', description='Put Videos')
    shareparser = subparsers.add_parser('share', description='Get Shareable Link')
    
    getparser.add_argument('video_file', type=str, help='video file')
    getparser.set_defaults(func=get_action)

    putparser.add_argument('video_file', type=str, nargs='*', help='video file(s)')
    putparser.add_argument('--dest_name', type=str, help='folder of video files')
    putparser.set_defaults(func=put_action)
    
    shareparser.add_argument('video_file', type=str, nargs='*', help='video file(s)')
    shareparser.set_defaults(func=share_action)
    
    args = parser.parse_args()
    args.func(args)


def get_action(args):
    print('GET: {} {}'.format(args.folder, args.video_file))
    gdrive = create_gdrive(args)
    file_path = os.path.join(args.folder, args.video_file)
    file_id = find_fileid(gdrive, file_path)
    if not file_id:
        print('Could not find file: {}'.format(file_path))
    else:
        video = gdrive.CreateFile({'id': file_id})
        video.GetContentFile(args.video_file)
   

def put_action(args):
    gdrive = create_gdrive(args)
    file_id = find_fileid(gdrive, args.folder)
    for video_file in args.video_file:
        print('PUT: {} {}'.format(args.folder, video_file))
        put_fileid = find_fileid_by_parent(gdrive, os.path.basename(video_file), file_id)
        if put_fileid:
            print('  ALREADY EXISTS ... skipping')
            continue
        
        title = args.dest_name
        if not title:
            title = os.path.basename(video_file)
        video = gdrive.CreateFile({'title': title, 'mimeType': 'video/mp4', 'parents': [{'id': file_id}]})
        video.SetContentFile(video_file)
        video.Upload()


def share_action(args):
    gdrive = create_gdrive(args)
    file_id = find_fileid(gdrive, args.folder)
    results = {}
    for video_file in args.video_file:
        print('SHARE: {} {}'.format(args.folder, video_file))
        path = os.path.join(args.folder, video_file)
        fileid = find_fileid(gdrive, path)
        if fileid:
            file = gdrive.CreateFile({'id': fileid})
            permission = file.InsertPermission({'type': 'anyone', 'value': 'anyone', 'role': 'reader'})
            file.FetchMetadata()
            results[video_file] = file['alternateLink']
    print(json.dumps(results))
   
 
def create_gdrive(args):  
    gauth = GoogleAuth()
    gauth.LoadCredentialsFile("gdrive_creds.txt")
    if gauth.credentials is None:
        gauth.LocalWebserverAuth() # client_secrets.json need to be in the same directory as the script
    elif gauth.access_token_expired:
        gauth.Refresh()
    else:
        gauth.Authorize()
    gauth.SaveCredentialsFile("gdrive_creds.txt")
    return GoogleDrive(gauth)


def find_fileid(gdrive, path):
    file_id = None
    parts = os.path.normpath(path).split('/')
    #print(parts)
    for name in parts:
        file_id = find_fileid_by_parent(gdrive, name, file_id)
        #print("found: file_id = {}".format(file_id))
    return file_id


def find_fileid_by_parent(gdrive, name, parent=None):
    #print("searching for {}".format(name))
    if parent is None:
        parent = 'root'
    file_id = None
    fileList = gdrive.ListFile({'q': "'{}' in parents and trashed=false".format(parent)}).GetList()
    for file in fileList:
        #print('Title: {}, ID: {}'.format(file['title'], file['id']))
        # Get the folder ID that you want
        if file['title'] == name:
            file_id = file['id']
            #print(file)
            #break
    return file_id



if __name__ == '__main__':
    main()
    
