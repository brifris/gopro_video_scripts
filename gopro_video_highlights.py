#!/usr/bin/python3

import argparse, datetime, os, glob, re, subprocess, sys, yaml

#
# manifest format notes:
# overlay: True    # causes 'note' field to overlay as text on the video segment
# convert:
#   scale: 1920x1080    # frame size
#   frame_rate: 30      # frames per second
#   crop: "w:h:x:y"     # crop to width:height starting at x:y (from upper left)
#

def main():
    parser = argparse.ArgumentParser(description='Cut and Splice highlight reels')
    parser.add_argument('--tmp_path', type=str, default='.', help='temporary directory')
    parser.add_argument('--extract_only', action='store_true', help='only extract the highlight segments into separate files')
    parser.add_argument('--trim', action='store_true', help='reencodes the extracted clips to trim to exact time offsets instead of just at keyframes (slower)')
    parser.add_argument('--overwrite', action='store_true', help='overwrites existing highlight videos with same name, without prompting')
    parser.add_argument('--dry_run', action='store_true', help='just say what segments would be selected')
    parser.add_argument('--per_subject', action='store_true', help='highlights for all available subjects.  If used --subject is ignored')
    parser.add_argument('--subject', type=str, action='append', help='subject(s) to select.  Default is any/all.')
    parser.add_argument('--action', type=str, action='append', help='action(s) to select.  Default is any/all.')
    parser.add_argument('--quality', type=int, default=0, help='numerical quality value to filter out segments less than this')
    parser.add_argument('--note', type=str, default='', help='note matching regex')
    parser.add_argument('manifest', type=str, help='yaml file describing video segments')
    args = parser.parse_args()

    if args.dry_run:
        args.extract_only = True

    with open(args.manifest, 'r') as fin:
        manifest = yaml.load(fin, Loader=yaml.Loader)
        
    subjects = set()
    if args.per_subject:
        for input in manifest['input']:
            for segment in input['segments']:
                if 'subject' in segment:
                    subject = segment['subject']
                    if subject is not None:
                        subjects.add(subject)
    else:
        if args.subject:
            [subjects.add(x) for x in args.subject]
        else:
            subjects.add(None)
    
    for subject in subjects:
        if not subject:
            args.subject = None
        else:
            args.subject = [subject]
        print("args.subject = {}".format(args.subject))   
        (reencode_needed, segment_files) = extract_input_segments(args, manifest)
        if len(segment_files) == 0:
            print("no matching segments: {}".format(subject))
            continue
        if not args.extract_only:
            if reencode_needed:
                concat_input_segments_reencode(args, manifest, segment_files)
            else:
                concat_input_segments(args, manifest, segment_files)
    
    
def extract_input_segments(args, manifest):
    #print(manifest)
    segment_id = -1
    output_file_prefix = manifest['output']['filename']
    segment_files = []
    reencode_needed = 'convert' in manifest
    total_time = 0.0
    for input in manifest['input']:
        src = input['src']
        for segment in input['segments']:
            if args.subject and ('subject' not in segment or segment['subject'] not in args.subject):
                #print("skipping due to subject: {}".format(segment))
                continue
            if args.action and ('action' not in segment or segment['action'] not in args.action):
                #print("skipping due to action: {}".format(segment))
                continue
            if args.quality and ('quality' not in segment or int(segment['quality']) < int(args.quality)):
                #print("skipping due to quality: {}".format(segment))
                continue
            if 'note' in segment and not re.search(args.note, segment['note']):
                #print("skipping due to note: {}".format(segment))
                continue
                
            total_time += time_to_tag(segment['to']) - time_to_tag(segment['from'])
            if args.dry_run:
                print("select: {} -> {}  {:<45}  {}".format(segment['from'], segment['to'], os.path.basename(src), segment.get('note', '')))
                continue
                
            segment_id += 1
            output_file = "{}-{:05}.mp4".format(output_file_prefix, segment_id)
            if "text" in segment:
                #continue
                segment_files.append(output_file)
                reencode_needed = True
                
                size = "1920x1080"
                if "size" in segment:
                    size = segment["size"]
                duration = 1
                if "duration" in segment:
                    duration = segment["duration"]
                fps = 30
                if "fps" in segment:
                    fps = segment["fps"]
                    
                cmd_args = ['ffmpeg', '-y', '-t', '1', '-f', 'lavfi', '-i', 'anullsrc=channel_layout=stereo:sample_rate=44100', '-f', 'lavfi', '-i', 'color=size={}:duration={}:rate={}:color=white'.format(size, duration, fps), '-vf', 'drawtext=fontfile=/usr/share/fonts/truetype/freefont/FreeSans.ttf:fontsize=64:fontcolor=black:x=(w-text_w)/2:y=(h-text_h)/2:text=\'{}\''.format(segment['text']), output_file]
                
                print(cmd_args)
                subprocess.run(cmd_args, shell=False, check=True)
                
            else:
                segment_files.append(output_file)
                cmd_args = ['ffmpeg', '-y', '-ss', segment['from'], '-to', segment['to'], '-i', src]
                if not args.trim:
                    cmd_args.append('-c')
                    cmd_args.append('copy')
                    cmd_args.append('-avoid_negative_ts')
                    cmd_args.append('1')
                cmd_args.append(output_file)
                print(cmd_args)
                subprocess.run(cmd_args, shell=False, check=True)
            
                overlay = 'overlay' in manifest and manifest['overlay']
                fade = 'fade' in manifest and manifest['fade']
                crop = 'crop' in segment
                if overlay or crop:
                    tmp_output_file = "tmp-" + output_file
                    os.rename(output_file, tmp_output_file)
                    print("NOTE: " + segment['note'])
                    
                    cmd_args = ['ffmpeg', '-y', '-i', tmp_output_file]
                    
                    if crop:
                        cmd_args.append('-filter:v')
                        cmd_args.append('crop=' + segment['crop'] + ':1') # tailing :1 is for keep_aspect=1
                    
                    if overlay:
                        cmd_args.append('-vf')
                        cmd_args.append('drawtext=fontfile=/usr/share/fonts/truetype/freefont/FreeSans.ttf: fontsize=28: text=\'' + segment['note'] + '\': fontcolor=white: box=1: boxcolor=black@0.5: boxborderw=5: x=10: y=10')

                    cmd_args.append('-codec:a')
                    cmd_args.append('copy')
                    cmd_args.append(output_file)
                    
                    print(cmd_args)
                    subprocess.run(cmd_args, shell=False, check=True)
                    os.remove(tmp_output_file)
                    
                if fade:
                    tmp_output_file = "tmp-" + output_file
                    os.rename(output_file, tmp_output_file)
                    
                    cmd_args = ['ffprobe -v error -show_format ' + tmp_output_file + ' | grep "duration="']
                    completedproc = subprocess.run(cmd_args, shell=True, check=True, capture_output=True, encoding='utf-8')
                    duration = float(completedproc.stdout.strip().split('=')[1]) - 1.0
                    print("duration = {}".format(duration))
                    
                    cmd_args = ['ffmpeg', '-y', '-i', tmp_output_file]
                    cmd_args.append('-filter_complex')
                    cmd_args.append('fade=t=in:st=0:d=1,fade=t=out:st={}:d=1'.format(duration))
                    cmd_args.append(output_file)
                    
                    print(cmd_args)
                    subprocess.run(cmd_args, shell=False, check=True)
                    os.remove(tmp_output_file)
                    
                

                #if 'convert' in manifest:
                #    convert = manifest['convert']
                #    scale = '1920x1080'
                #    frame_rate = 30
                #    if 'scale' in convert:
                #        scale = convert['scale']
                #    if 'frame_rate' in convert:
                #        frame_rate = convert['frame_rate']
                #    tmp_output_file = "tmp-" + output_file
                #    os.rename(output_file, tmp_output_file)
                #    cmd_args = ['ffmpeg', '-i', tmp_output_file, '-preset', 'superfast', '-s', scale, '-r', str(frame_rate), '-c:a', 'copy',  output_file]
                #    print(cmd_args)
                #    subprocess.run(cmd_args, shell=False, check=True)
                #    os.remove(tmp_output_file)

    print("total time selected: {}".format(tag_to_time(total_time)))
    return (reencode_needed, segment_files)


def tag_to_time(tag):
    # convert floating point time tag to 00:00:00 time format
    dt = datetime.datetime.fromtimestamp(float(tag), datetime.timezone.utc)
    return dt.strftime("%H:%M:%S") 


def time_to_tag(timestr):
    # convert 00:00:00 time format to floating point time tag
    return (datetime.datetime.strptime(timestr, '%H:%M:%S') - datetime.datetime(1900, 1, 1)).total_seconds()


def concat_input_segments(args, manifest, segment_files):
    output_file = manifest['output']['filename']
    if args.subject:
        ext = os.path.splitext(output_file)
        output_file = ext[0] + "-" + "_".join(args.subject).replace(' ', '_') + ext[1]
    if args.action:
        ext = os.path.splitext(output_file)
        output_file = ext[0] + "-" + "_".join(args.action) + ext[1]
    if args.quality > 0:
        ext = os.path.splitext(output_file)
        output_file = ext[0] + "-" + "q" + str(args.quality) + ext[1]
    
        
    concat_manifest = "{}.txt".format(output_file)
    with open(concat_manifest, 'w') as f:
        for file in segment_files:
            f.write("file '{}'\n".format(file))
    cmd_args = ['ffmpeg', '-f', 'concat', '-safe', '0', '-i', concat_manifest, '-c', 'copy', output_file]
    if args.overwrite:
        cmd_args.append('-y')
    print(cmd_args)
    subprocess.run(cmd_args, shell=False, check=True)
    
    os.remove(concat_manifest)
    for file in segment_files:
        os.remove(file)
    
    
def concat_input_segments_reencode(args, manifest, segment_files):
    output_file = manifest['output']['filename']
    
    scale = '1920x1080'
    frame_rate = 30
    pix_fmt = 'yuvj420p'
    audio_opts = 'aformat=sample_rates=44100:channel_layouts=stereo,asetpts=PTS-STARTPTS'
    crop = ""
    if 'convert' in manifest:
        convert = manifest['convert']
        if 'scale' in convert:
            scale = convert['scale']
        if 'frame_rate' in convert:
            frame_rate = convert['frame_rate']
        if 'crop' in convert:
            crop = "crop=" + convert['crop'] + ","

    cmd_args = ['ffmpeg']
    filter = []
    for file in segment_files:
        cmd_args.append('-i')
        cmd_args.append(file)
    cmd_args.append('-filter_complex')
    for idx, file in enumerate(segment_files):
        filter.append('[{}:v]fps={},scale={},format={},{}setpts=PTS-STARTPTS[v{}];'.format(idx, frame_rate, scale, pix_fmt, crop, idx))
        filter.append('[{}:a]{}[a{}];'.format(idx, audio_opts, idx))
    for idx, file in enumerate(segment_files):
        filter.append("[v{}][a{}]".format(idx, idx))
    filter.append("concat=n={}:v=1:a=1".format(len(segment_files)))
    
    cmd_args.append("".join(filter))
    cmd_args.append(output_file)
    if args.overwrite:
        cmd_args.append('-y')
    print(cmd_args)
    subprocess.run(cmd_args, shell=False, check=True)
    
    for file in segment_files:
        os.remove(file)




if __name__ == '__main__':
    main()
    
