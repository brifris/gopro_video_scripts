#!/usr/bin/python3

import argparse, os, glob, subprocess

###
#
# usage example:
# cd /mnt/Videos/GCH-softball/GCH-2020-07-03
# gopro_video_merge TNT-2020-07-03-GM6 40 41 42 43 44 45 46
#
# NOTE: If using ffmpeg snap version and your videos aren't in your home directory...
#   Create a bind mount to data partition under /mnt to fake out stupid ffmpeg snap security permissions
# sudo mount --bind /data/mystuff/Videos/ /mnt/Videos/
#
###


def main():
    parser = argparse.ArgumentParser(description='Merge GoPro videos into a single video')
    parser.add_argument('--video_path', type=str, default='.', help='directory path to all of the videos')
    parser.add_argument('--output_path', type=str, default='.', help='output directory path for output video')
    parser.add_argument('--skip_existing', action='store_true', help='skip processing files where output already exists')
    parser.add_argument('--camera', type=str, default='8black', help='go pro camera model, supported=8black,hero5,hero3')
    parser.add_argument('--res', type=str, action='append', help='high|low resolution.  Low res we will select GL<seq><vid_id>.LRV files.  High res we will select GH<seq><vid_id>.MP4 files.  Default is both high and low.')
    parser.add_argument('output', type=str, help='output video name')
    parser.add_argument('gopro_vid_id', type=str, nargs='*', help='GoPro video ID: GL<seq><vid_id>.LRV, for example GL030026.LRV -> 26.')
    args = parser.parse_args()

    res_list = args.res
    if not args.res:
        res_list = ["low", "high"]
    for res in res_list:
        output_video = os.path.join(args.output_path, "{}-{}.mp4".format(args.output, res))
        if os.path.exists(output_video):
            if not args.skip_existing:
                raise Exception("Output video already exists!  {}".format(output_video))
            print("skipping merge because output video already exists: {}".format(output_video))
            continue
        build_manifest(args, res)
        build_video(args, res)


def build_manifest(args, res):
    print("building ffmpeg manifest to concatenate videos")
    print(res)
    print(args.gopro_vid_id)
    if res == 'low':
        res_prefix = 'L'
        ext = 'LRV'
    elif res == 'high':
        res_prefix = 'H'
        ext = 'MP4'
    else:
        raise Exception("Invalid resolution.  Only low or high are supported")
    
    path = os.path.abspath(args.video_path)
    with open(os.path.join('.', "{}-{}.txt".format(args.output, res)), 'w') as f:
        for vid_id in args.gopro_vid_id:
            if args.camera == '8black':
                for file in sorted(glob.glob('G{0}*{1}.{2}'.format(res_prefix, vid_id, ext))):
                    f.write("file '{}'\n".format(os.path.join(path, file)))
            elif args.camera in ['hero5', 'hero3']:
                for file in sorted(glob.glob('GOPR*{0}.{1}'.format(vid_id, ext))):
                    f.write("file '{}'\n".format(os.path.join(path, file)))
                for file in sorted(glob.glob('GP*{0}.{1}'.format(vid_id, ext))):
                    f.write("file '{}'\n".format(os.path.join(path, file)))
            else:
                raise Exception("Camera {} is not a supported model".format(args.camera))


def build_video(args, res):
    manifest = "{}-{}.txt".format(args.output, res)
    output_video = os.path.join(args.output_path, "{}-{}.mp4".format(args.output, res))
    #TODO: occasionally see warnings about these settings
    # '-analyzeduration', '2147483647', '-probesize', '21477483647',
    cmd_args = ['ffmpeg', '-f', 'concat', '-safe', '0', '-i', manifest, '-c', 'copy', output_video]
    print(cmd_args)
    subprocess.run(cmd_args, shell=False, cwd=args.video_path, check=True)


if __name__ == '__main__':
    main()

