#!/usr/bin/python3

import argparse, os, glob, subprocess, sys, json, yaml, time
import jsonschema
import requests

###
#
# usage example:
# cd /mnt/Videos/GCH-softball/
#
# reads from a yaml manifest the description of the game videos and performs the whole processing pipeline
# to get them ready to go:
#   - merge         stich together the gopro files into a single game video file
#   - filter        convert to YouTube friendly size/res/fps/etc
#   - upload        upload to YouTube
#   - index         update GitLab index.html page
#
# example manifest
#
# config:
#   output_path: /media/stuff/Videos/2020-Fall-GCH/2020-09-19-Severn
#   camera: 8black
#   video_prefix: 2020-09-19-Severn
#   youtube_res: 720p
# games:
# - name: GM1-Firebirds
#   team: Firebird 16u Hardy-Kelley
#   result: W 10-0
#   gopro_ids: [163, 164]
# - name: GM2-VaMizuno
#   team: Team Va Mizuno Peregrino 16u
#   result: W 5-2
#   gopro_ids: [170]
#
# NOTE: Create a bind mount to data partition under /mnt to fake out stupid ffmpeg snap security permissions
# sudo mount --bind /data/stuff/Videos/ /mnt/Videos/
#
###


def main():
    parser = argparse.ArgumentParser(description='Full GoPro videos processing pipeline')
    parser.add_argument('--validate', action='store_true', help='validate manifest format')
    parser.add_argument('--dry_run', action='store_true', help='just say what processing we would do')
    parser.add_argument('--skip_merge', action='store_true', help='skip merge phase')
    parser.add_argument('--skip_boxscore', action='store_true', help='skip boxscore phase')
    parser.add_argument('--skip_filter', action='store_true', help='skip filter phase')
    parser.add_argument('--skip_highlights', action='store_true', help='skip highlights phase')
    parser.add_argument('--skip_upload', action='store_true', help='skip youtube upload phase')
    parser.add_argument('--skip_gdrive_upload', action='store_true', help='skip gdrive upload phase')
    parser.add_argument('--skip_index', action='store_true', help='skip index phase')
    parser.add_argument('manifest', type=str, help='yaml file describing video segments and how to process')
    args = parser.parse_args()

    with open(args.manifest, 'r') as fin:
        manifest = yaml.load(fin, Loader=yaml.Loader)

    if args.validate:
        validate(manifest)
        return

    output_path = manifest['config']['output_path']
    if not os.path.exists(output_path):
        os.makedirs(output_path)
                
    if not args.skip_merge:
        merge(args, manifest)
    if not args.skip_boxscore:
        boxscore(args, manifest)
    if not args.skip_filter:
        filter(args, manifest)
    if not args.skip_highlights:
        highlights(args, manifest)
    if not args.skip_upload:
        upload(args, manifest)
    if not args.skip_gdrive_upload:
        upload_gdrive(args, manifest)
    if not args.skip_index:
        update_index(args, manifest)

    # save a copy of the manifest to the output path with all of the videos        
    save_manifest(args, manifest, os.path.join(output_path, 'videos.yaml'))


def validate(manifest):
    schema = """
    type: object
    properties:
      config:
        type: object
        properties:
          boxscore:
            type: string
          camera:
            type: string
          index_path:
            type: string
          output_path:
            type: string
          video_prefix:
            type: string
          tournament:
            type: string
          result:
            type: [string, 'null']
          youtube_res:
            type: string
        required: ["camera", "index_path", "output_path", "video_prefix"]
      games:
        type: array
        items:
          type: object
          properties:
            gopro_ids:
              type: array
              items:
                type: string
            name:
              type: string
            team:
              type: [string, 'null']
            result:
              type: [string, 'null']
            youtube_id:
              type: [string, 'null']
            creation_time:
              type: [string, 'null']
          required: ["gopro_ids", "name"]
    required: ["config", "games"]
    """
    jsonschema.validate(manifest, yaml.load(schema, Loader=yaml.Loader))


def merge(args, manifest):
    print("=== Merging ===")
    output_path = manifest['config']['output_path']
    camera = manifest['config']['camera']
    video_prefix = manifest['config']['video_prefix']
    for game in manifest['games']:
        video_name = video_prefix + '-' + game['name']
        update_creation_time(args, game)
        cmd_args = ['gopro_video_merge', '--skip_existing', '--output_path', output_path, '--camera', camera, video_name]
        cmd_args += game['gopro_ids']
        print(cmd_args)
        if not args.dry_run:
            subprocess.run(cmd_args, shell=False, check=True)
    save_manifest(args, manifest)


def update_creation_time(args, game):
    # GOPRO video stream include a creation time stamp TAG (multiple actually)
    # we will take the first one, because our follow-on processing will lose these tags
    cmd_args = ['gopro_video_details {} 2>&1 | grep creation_time | head -1'.format(sorted(glob.glob('G*' + game['gopro_ids'][0] + '.MP4'))[0])]
    creation_time = subprocess.check_output(cmd_args, shell=True, encoding='utf-8').strip()
    # value will look like: 'TAG:creation_time=2020-10-04T07:34:10.000000Z'
    game['creation_time'] = creation_time.split('=')[1]


def boxscore(args, manifest):
    print("=== BoxScore ===")
    output_path = manifest['config']['output_path']
    for game in manifest['games']:
        rtga_yaml = get_rtga_yaml(manifest, game)
        if rtga_yaml is None:
            print("Skipping boxscore video generation for {}".format(game['name']))
            continue
        
        youtube_res = manifest['config']['youtube_res']
        video_prefix = manifest['config']['video_prefix']
        input_video = os.path.join(output_path, "{}-{}-high.mp4".format(video_prefix, game['name']))      
        output_video = os.path.join(output_path, "{}-{}-{}.mp4".format(video_prefix, game['name'], youtube_res))
        cmd_args = ['gopro_video_boxscore', '--skip_existing', '--config', manifest['config']['boxscore'], rtga_yaml, input_video, output_video]
        print(cmd_args)
        if not args.dry_run:
            subprocess.run(cmd_args, shell=False, check=True)


def get_rtga_yaml(manifest, game):
    output_path = manifest['config']['output_path']
    rtga_url = manifest['config']['rtga_url']
    name = game['name']
    if 'rtga_name' in game:
        name = game['rtga_name']
    rtga_yaml = os.path.join(output_path, 'rtga-' + game['name'] + '.yaml')
    if os.path.exists(rtga_yaml):
        print("skipping download rtga data, already exists: {}".format(rtga_yaml))
    else:
        yaml_url = "{}game/{}/yaml".format(rtga_url, name)
        print(yaml_url)
        r = requests.get(yaml_url)
        if not r.ok:
            print("Unable to download rtga data from {}, {}".format(yaml_url, r))
            return None
        with open(rtga_yaml, 'w') as fout:
            fout.write(r.text)
    return rtga_yaml


def highlights(args, manifest):
    print("=== Highlights ===")
    output_path = manifest['config']['output_path']
    youtube_res = manifest['config']['youtube_res']
    for game in manifest['games']:
        rtga_yaml = get_rtga_yaml(manifest, game)
        if rtga_yaml is None:
            print("Skipping highlights video generation for {}".format(game['name']))
            continue
        
        with open(rtga_yaml, 'r') as fin:
            rtga = yaml.load(fin, Loader=yaml.Loader)
        
        # Convert RTGA to Tagging yaml format and generate the highlights!
        video_prefix = manifest['config']['video_prefix']
        tagging = {}
        tagging['input'] = [{
            "src": os.path.join(output_path, "{}-{}-{}.mp4".format(video_prefix, game['name'], youtube_res)),
            "segments": rtga['state']['segments']
        }]
        tagging['output'] = {
            "filename": "{}-{}-{}_highlights.mp4".format(video_prefix, game['name'], youtube_res)
        }
        tag_yaml = os.path.join(output_path, 'tags-' + game['name'] + '.yaml')
        with open(tag_yaml, 'w') as fout:
            output = yaml.dump(tagging, Dumper=yaml.Dumper, default_flow_style=None, width=1000)
            fout.write(output)
        
        cmd_args = ['gopro_video_tag', 'segments', '--tag_file', tag_yaml, '--to_inc', '1']
        print(cmd_args)
        subprocess.run(cmd_args, shell=False, cwd=output_path, check=False)
            
        cmd_args = ['gopro_video_highlights', tag_yaml, '--overwrite', '--trim', '--quality', '1']
        print(cmd_args)
        if not args.dry_run:
            subprocess.run(cmd_args, shell=False, cwd=output_path, check=False)
            
        cmd_args = ['gopro_video_highlights', tag_yaml, '--overwrite', '--trim', '--quality', '2', '--per_subject']
        print(cmd_args)
        if not args.dry_run:
            subprocess.run(cmd_args, shell=False, cwd=output_path, check=False)
            
        #cmd_args = ['gopro_video_filter', '--res', youtube_res, '--fast', '{}-{}-high_highlights-q1.mp4'.format(video_prefix, game['name'])]
        #print(cmd_args)
        #if not args.dry_run:
        #    subprocess.run(cmd_args, shell=False, cwd=output_path, check=False)
    
            
def filter(args, manifest):
    print("=== Filtering ===")
    # Still do after boxscore incase we didn't have RTGA data and couldn't overlay the boxscore
    # This would then create the 720p video, just without the boxscore
    # TODO: consider also converting to 1080p for archives
    cmd_args = ['gopro_video_filter', '--all', '--skip_existing', '--fast']
    print(cmd_args)
    if not args.dry_run:
        output_path = manifest['config']['output_path']
        subprocess.run(cmd_args, shell=False, cwd=output_path, check=True)


def upload(args, manifest):
    print("=== Uploading ===")
    output_path = manifest['config']['output_path']
    youtube_res = manifest['config']['youtube_res']
    video_prefix = manifest['config']['video_prefix']
    for game in manifest['games']:
        if 'youtube_id' in game and game['youtube_id']:
            print('already uploaded {}, youtube id = {}'.format(game['name'], game['youtube_id']))
        else:
            video = os.path.join(output_path, video_prefix + '-' + game['name'] + '-' + youtube_res + '.mp4')
            if not os.path.exists(video):
                raise Exception("Unable to find video file!: {}".format(video))

            # create meta file
            meta = {
              "description": '{}\n{}'.format(game['team'], game['result']),
              "privacy": 'unlisted'
            }
            meta_file = os.path.join(output_path, 'youtube-' + video_prefix + '-' + game['name'] + '-' + youtube_res + '.json')
            with open(meta_file, 'w') as fout:
                fout.write(json.dumps(meta))

            output_id_file = meta_file + '.id'
            # TODO: make path to youtube_uploader_selenium configurable
            cmd_args = ['python3', './youtube_uploader_selenium/upload.py', '--video', video, '--meta', meta_file, '--output_id', output_id_file]
            print(cmd_args)
            if not args.dry_run:
                # running from ~/.youtube directory because credentials are stored there
                subprocess.run(cmd_args, shell=False, cwd='{}/.youtube/'.format(os.environ['HOME']), check=True)
                os.remove(meta_file)
            
                with open(output_id_file, 'r') as f:
                    game['youtube_id'] = f.read().strip()
                os.remove(output_id_file)
        
        if 'youtube_abridged_id' in game and game['youtube_abridged_id']:
            print('already uploaded {}, youtube abridged id = {}'.format(game['name'], game['youtube_abridged_id']))
        else:    
            video = os.path.join(output_path, video_prefix + '-' + game['name'] + '-' + youtube_res + '_highlights-q1.mp4')
            if not os.path.exists(video):
                print("Unable to find abridged video file!: {}".format(video))
                continue

            # create meta file
            meta = {
              "description": '{}\n{}\nAbridged version'.format(game['team'], game['result']),
              "privacy": 'unlisted'
            }
            meta_file = os.path.join(output_path, 'youtube-' + video_prefix + '-' + game['name'] + '-' + youtube_res + '_highlights-q1.json')
            with open(meta_file, 'w') as fout:
                fout.write(json.dumps(meta))

            output_id_file = meta_file + '.id'
            cmd_args = ['python3', './youtube_uploader_selenium/upload.py', '--video', video, '--meta', meta_file, '--output_id', output_id_file]
            print(cmd_args)
            if not args.dry_run:
                # running from ~/.youtube directory because credentials are stored there
                subprocess.run(cmd_args, shell=False, cwd='{}/.youtube/'.format(os.environ['HOME']), check=True)
                os.remove(meta_file)
            
                with open(output_id_file, 'r') as f:
                    game['youtube_abridged_id'] = f.read().strip()
                os.remove(output_id_file)
    
    save_manifest(args, manifest)


def save_manifest(args, manifest, output_file=None):
    if output_file is None:
        output_file = args.manifest
    with open(output_file, 'w') as fout:
        output = yaml.dump(manifest, Dumper=yaml.Dumper, width=1000)
        fout.write(output)


def upload_gdrive(args, manifest):
    print("=== Uploading Highlights to GDrive ===")
    output_path = manifest['config']['output_path']
    youtube_res = manifest['config']['youtube_res']
    video_prefix = manifest['config']['video_prefix']
    gdrive_folder = manifest['config']['gdrive_folder']
    for game in manifest['games']:
        print("= " + game['name'] + ' =')
        cmd_args = ['gopro_video_gdrive', '--folder', gdrive_folder, 'put']
        highlights = []
        for highlight_video in glob.glob(os.path.join(output_path, '{}-{}-{}_highlights*-q2.mp4'.format(video_prefix, game['name'], youtube_res))):
            highlights.append(highlight_video)
            
        if len(highlights) <= 0:
            continue
            
        for x in highlights:
            cmd_args.append(x)
        game['highlights'] = highlights    
        print(cmd_args)
        if not args.dry_run:
            subprocess.run(cmd_args, shell=False, check=True)
        save_manifest(args, manifest)
        
        cmd_args = ['gopro_video_gdrive', '--folder', gdrive_folder, 'share']
        for x in highlights:
            cmd_args.append(os.path.basename(x))
        print(cmd_args)
        if not args.dry_run:
            subprocess.run(cmd_args, shell=False, check=True)
            links = subprocess.check_output(cmd_args, shell=False, encoding='utf-8').strip().split('\n')[-1]
            #print(links)
            linkmap = json.loads(links)
            game['highlightlinks'] = linkmap
            save_manifest(args, manifest)
        
    pass


def update_index(args, manifest):
    print("=== Updating Index ===")
    index_path = manifest['config']['index_path']
    index_file = os.path.join(index_path, 'index.html')
    tournament = manifest['config']['tournament']
    result = manifest['config']['result']
    background = manifest['config']['index_title_background']
    foreground = manifest['config']['index_title_foreground']
    
    sb = []
    sb.append('<ul class="list-group">')
    sb.append('  <li class="list-group-item" style="background-color:{}; color:{};">'.format(background, foreground))
    sb.append('    {}<br>'.format(tournament))
    sb.append('    {}'.format(result or ''))
    sb.append('  </li>')

    for game in manifest['games']:
        sb.append('  <li class="list-group-item">')
        sb.append('    <h3>{}</h3>'.format(game['team']))
        sb.append('    {}<br>'.format(game['result'] or ''))
        youtube_id = ''
        if 'youtube_id' in game:
            youtube_id = game['youtube_id']
        sb.append('    <a href="https://youtu.be/{0}">https://youtu.be/{0}</a> (Full)<br>'.format(youtube_id))
        if 'youtube_abridged_id' in game and game['youtube_abridged_id'] != 'null':
            youtube_abridged_id = game['youtube_abridged_id']
            sb.append('    <a href="https://youtu.be/{0}">https://youtu.be/{0}</a> (Short: 5-10 mins)<br>'.format(youtube_abridged_id))
        if 'highlightlinks' in game:
            sb.append('    <h4>Per Player Highlights</h4>')
            sb.append('    <ul>')
            for name in game['highlightlinks']:
                link = game['highlightlinks'][name]
                sb.append('    <li><a href="{}">{}</a></li>'.format(link, name))
            sb.append('    </ul>')
        sb.append('  </li>')

    sb.append('</ul>')
  
    with open(index_file, 'r') as fin:
        index = fin.read()
    
    INSERT_MARKER = '<!-- INSERT NEW GAMES HERE -->'
    index = index.replace(INSERT_MARKER, INSERT_MARKER + '\n\n' + '\n'.join(sb))
    #print(index)
    
    with open(index_file, 'w') as fout:
        fout.write(index)
    
    cmd_args = ['git', 'add', 'index.html']
    print(cmd_args)
    if not args.dry_run:
        subprocess.run(cmd_args, shell=False, cwd=index_path, check=True)

    cmd_args = ['git', 'commit', '-m', 'auto update ' + str(time.time())]
    print(cmd_args)
    if not args.dry_run:
        subprocess.run(cmd_args, shell=False, cwd=index_path, check=True)

    cmd_args = ['git', 'push']
    print(cmd_args)
    if not args.dry_run:
        subprocess.run(cmd_args, shell=False, cwd=index_path, check=True)

        

if __name__ == '__main__':
    main()

