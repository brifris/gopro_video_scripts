#!/usr/bin/python3

import argparse, os, sys, yaml
from urllib.parse import urlparse
import datetime

#
# Typically this is called from the VLC extension, but for that to work with the snap version of VLC
# any changes here must be copied to ~/snap/vlc/current/.local/share/vlc
#

def main():
    parser = argparse.ArgumentParser(description='Work with Video Tags')
    subparsers = parser.add_subparsers(title='subcommands', description='valid subcommands')
    updateparser = subparsers.add_parser('update', description='Update Tags')
    findparser = subparsers.add_parser('find', description='Find start of Tag')
    defaultsparser = subparsers.add_parser('defaults', description='Update tags with additional default properties')
    segmentsparser = subparsers.add_parser('segments', description='Apply common transformations to all segments')
    
    updateparser.add_argument('video_uri', type=str, help='URI for video file')
    updateparser.add_argument('--tag_in', type=str, help='time to start tag')
    updateparser.add_argument('--tag_out', type=str, help='time to stop tag')
    updateparser.set_defaults(func=update_action)
    
    findparser.add_argument('video_uri', type=str, help='URI for video file')
    findparser.add_argument('tag_index', type=int, help='0-based tag index to find')
    findparser.set_defaults(func=find_action)
    
    defaultsparser.add_argument('--tag_file', type=str, default='tagging-updates.yaml', help='tagging yaml filename')
    defaultsparser.add_argument('--set', metavar='KEY=VALUE', nargs='+', help='Set key=value on each segment')
    defaultsparser.set_defaults(func=defaults_action)
    
    segmentsparser.add_argument('--tag_file', type=str, default='tagging-updates.yaml', help='tagging yaml filename')
    segmentsparser.add_argument('--from_inc', type=int, default=0, help='Number of seconds to increment all from times by')
    segmentsparser.add_argument('--to_inc', type=int, default=0, help='Number of seconds to increment all to times by')
    segmentsparser.add_argument('--merge_threshold', type=int, default=3, help='Segments within these number of seconds will be merged')
    segmentsparser.set_defaults(func=segments_action)
    
    args = parser.parse_args()
    args.func(args)


def update_action(args):
    print(args.video_uri)
    print(args.tag_in)
    print(args.tag_out)
    uri = urlparse(args.video_uri)
    path = os.path.dirname(uri.path)
    video_file = os.path.basename(uri.path)
    manifest_file = os.path.join(path, 'tagging-updates.yaml')
    print("manifest_file = " + manifest_file)
    manifest = None
    if os.path.exists(manifest_file):
        with open(manifest_file, 'r') as fin:
            manifest = yaml.load(fin, Loader=yaml.Loader)
    
    if manifest is None:
        manifest = {}
    if 'output' not in manifest:
        manifest["output"] = {
            "filename": "tagging-output.mp4"
        }
    if 'input' not in manifest:
        manifest["input"] = []
    
    last_input = find_input(manifest, video_file)
            
    if last_input is None:
        last_input = {
            "src": video_file,
            "segments": [{
                "from": "",
                "to": ""
            }]
        }
        manifest["input"].append(last_input)
        
    if args.tag_in:
        last_segment = None
        if len(last_input["segments"]) > 0:
            if not last_input["segments"][-1]["from"] or not last_input["segments"][-1]["to"]:
                last_segment = last_input["segments"][-1]
        if last_segment is None:
            last_segment = {
                "from": "",
                "to": ""
            }
            last_input["segments"].append(last_segment)
        last_segment["from"] = tag_to_time(args.tag_in)
        
    if args.tag_out:
        last_segment = None
        if len(last_input["segments"]) > 0:
            last_segment = last_input["segments"][-1]
        if last_segment is None:
            last_segment = {
                "from": "",
                "to": ""
            }
            last_input["segments"].append(last_segment)
        last_segment["to"] = tag_to_time(args.tag_out)
        
    print(manifest)
        
    with open(manifest_file, 'w') as fout:
        output = yaml.dump(manifest, Dumper=yaml.Dumper, default_flow_style=None, width=1000)
        fout.write(output)


def find_input(manifest, video_file):
    for input in manifest['input']:
        if input['src'] == video_file:
            return input


def tag_to_time(tag):
    # convert floating point time tag to 00:00:00 time format
    dt = datetime.datetime.fromtimestamp(float(tag), datetime.timezone.utc)
    return dt.strftime("%H:%M:%S") 


def time_to_tag(timestr):
    # convert 00:00:00 time format to floating point time tag
    return (datetime.datetime.strptime(timestr, '%H:%M:%S') - datetime.datetime(1900, 1, 1)).total_seconds()
    

def find_action(args):
    #print(args.video_uri)
    #print(args.tag_index)
    uri = urlparse(args.video_uri)
    path = os.path.dirname(uri.path)
    video_file = os.path.basename(uri.path)
    manifest_file = os.path.join(path, 'tagging-updates.yaml')
    #print("manifest_file = " + manifest_file)
    manifest = None
    if os.path.exists(manifest_file):
        with open(manifest_file, 'r') as fin:
            manifest = yaml.load(fin, Loader=yaml.Loader)
    
    if not manifest and 'input' not in manifest:
        return
    input = find_input(manifest, video_file)
    if input:
        if args.tag_index < len(input['segments']):
            segment = input['segments'][args.tag_index]
            print(time_to_tag(segment['from']))
            

def defaults_action(args):
    props = {}
    for kv in args.set:
        (k, v) = kv.strip().split('=')
        #print("{} = {}".format(k, v))
        if len(k) > 0:
            props[k] = v
            
    manifest = None
    with open(args.tag_file, 'r') as fin:
        manifest = yaml.load(fin, Loader=yaml.Loader)

    if manifest is None:
        manifest = {}
    if 'input' not in manifest:
        manifest["input"] = []
    for input in manifest['input']:
        if 'segments' in input:
            for segment in input['segments']:
                #print(segment)
                for prop in props.keys():
                    if prop not in segment:
                        segment[prop] = props[prop]

    #output = yaml.dump(manifest, Dumper=yaml.Dumper, default_flow_style=None, width=1000)
    #print(output)
        
    with open(args.tag_file, 'w') as fout:
        output = yaml.dump(manifest, Dumper=yaml.Dumper, default_flow_style=None, width=1000)
        fout.write(output)


def segments_action(args):
    manifest = None
    with open(args.tag_file, 'r') as fin:
        manifest = yaml.load(fin, Loader=yaml.Loader)

    if manifest is None:
        manifest = {}
    if 'input' not in manifest:
        manifest["input"] = []
    for input in manifest['input']:
        if 'segments' in input:
            newsegs = []
            last_to_tag = 0.0
            for segment in input['segments']:
                from_tag = time_to_tag(segment['from'])
                to_tag = time_to_tag(segment['to'])
                if to_tag - from_tag > 1.0:
                    from_tag += args.from_inc
                    to_tag += args.to_inc
                    if len(newsegs) > 0 and from_tag - last_to_tag <= args.merge_threshold:
                        print("merging:")
                        print("  A: {}".format(newsegs[-1]))
                        print("  B: {}".format(segment))
                        newsegs[-1]['to'] = tag_to_time(to_tag)
                        last_to_tag = to_tag
                        print("  C: {}".format(newsegs[-1]))
                    else:
                        segment['from'] = tag_to_time(from_tag)
                        segment['to'] = tag_to_time(to_tag)
                        if segment['from'] != segment['to']:
                            newsegs.append(segment)
                            last_to_tag = to_tag
            input['segments'] = newsegs

    with open(args.tag_file, 'w') as fout:
        output = yaml.dump(manifest, Dumper=yaml.Dumper, default_flow_style=None, width=1000)
        fout.write(output)


if __name__ == '__main__':
    main()
    
